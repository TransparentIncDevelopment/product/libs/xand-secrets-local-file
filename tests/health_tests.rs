use matches::assert_matches;
use test_utils::{config_for_tempfile, file_with_contents};
use xand_secrets::{CheckHealthError, SecretKeyValueStore};
use xand_secrets_local_file::{LocalFileSecretKeyValueStore, LocalFileSecretStoreConfiguration};

mod test_utils;

#[tokio::test]
async fn basic_healthy() {
    let file = file_with_contents(
        r#"
            some_key: some_value
            another_key: another_value
        "#,
    );

    let store = LocalFileSecretKeyValueStore::create_from_config(config_for_tempfile(&file));

    assert_matches!(store.check_health().await, Ok(()));
}

#[tokio::test]
async fn no_such_file() {
    let store =
        LocalFileSecretKeyValueStore::create_from_config(LocalFileSecretStoreConfiguration {
            yaml_file_path: String::from("./this/does/not/exist"),
        });

    assert_matches!(
        store.check_health().await,
        Err(CheckHealthError::Unreachable { internal_error: _ })
    );
}

#[tokio::test]
async fn invalid_yaml() {
    let file = file_with_contents(
        r#"
        foo: :
    "#,
    );

    let store = LocalFileSecretKeyValueStore::create_from_config(config_for_tempfile(&file));

    assert_matches!(
        store.check_health().await,
        Err(CheckHealthError::RemoteInternal { internal_error: _ })
    );
}
