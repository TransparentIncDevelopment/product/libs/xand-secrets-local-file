use matches::assert_matches;
use test_utils::{config_for_tempfile, file_with_contents};
use xand_secrets::{ExposeSecret, ReadSecretError, SecretKeyValueStore};
use xand_secrets_local_file::{LocalFileSecretKeyValueStore, LocalFileSecretStoreConfiguration};

mod test_utils;

#[tokio::test]
async fn basic_single_key() {
    let file = file_with_contents(
        r#"
        some_key: some_value 
    "#,
    );

    let store = LocalFileSecretKeyValueStore::create_from_config(config_for_tempfile(&file));
    assert_eq!(
        "some_value",
        store.read("some_key").await.unwrap().expose_secret()
    );
}

#[tokio::test]
async fn basic_multi_key() {
    let file = file_with_contents(
        r#"
        some_key: some_value
        another_key: another_value
    "#,
    );

    let store = LocalFileSecretKeyValueStore::create_from_config(config_for_tempfile(&file));
    assert_eq!(
        "some_value",
        store.read("some_key").await.unwrap().expose_secret()
    );
    assert_eq!(
        "another_value",
        store.read("another_key").await.unwrap().expose_secret()
    );
}

#[tokio::test]
async fn numeric_values() {
    let file = file_with_contents(
        r#"
        some_key: 1234
    "#,
    );

    let store = LocalFileSecretKeyValueStore::create_from_config(config_for_tempfile(&file));
    assert_eq!(
        "1234",
        store.read("some_key").await.unwrap().expose_secret()
    );
}

#[tokio::test]
async fn no_such_file() {
    let store =
        LocalFileSecretKeyValueStore::create_from_config(LocalFileSecretStoreConfiguration {
            yaml_file_path: String::from("./this/does/not/exist"),
        });
    assert_matches!(
        store.read("some_key").await,
        Err(ReadSecretError::Request { internal_error: _ })
    );
}

#[tokio::test]
async fn invalid_yaml() {
    let file = file_with_contents(
        r#"
        foo: :
    "#,
    );

    let store = LocalFileSecretKeyValueStore::create_from_config(config_for_tempfile(&file));
    assert_matches!(
        store.read("foo").await,
        Err(ReadSecretError::Request { internal_error: _ })
    );
}
